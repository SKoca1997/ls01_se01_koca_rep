import java.util.Scanner;

public class Zeichensortierer {

	//Zeichen werden hier eingegeben
	public static void sortiereZahlen() {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Geben sie die 1. Zahl ein: ");
		char lZeichen1 = myScanner.next().charAt(0);
		System.out.println("Geben sie die 2. Zahl ein: ");
		char lZahl2 = myScanner.next().charAt(0);
		System.out.println("Geben sie die 3. Zahl ein: ");
		char lZahl3 = myScanner.next().charAt(0);
		char lKleinsteZahl = holeKleinstezahl(lZeichen1, lZahl2, lZahl3);
		char lGroesteZahl = holeGroesteZahl(lZeichen1, lZahl2, lZahl3);
		char lMittlereZahl = holeMittlereZahl(lZeichen1, lZahl2, lZahl3);
		System.out.println(lKleinsteZahl + " " + lMittlereZahl + " " + lGroesteZahl);
	}

	//Zeichen werden hier sortiert
	public static char holeKleinstezahl(char lZahl1, char lZahl2, char lZahl3) {
		char kZahl = lZahl1;
		if (lZahl2 < kZahl) {
			kZahl = lZahl2;
		}
		if (lZahl3 < kZahl) {
			kZahl = lZahl3;
		}
		return kZahl;
	}

	public static char holeGroesteZahl(char lZahl1, char lZahl2, char lZahl3) {
		char gZahl = lZahl1;
		if (lZahl2 > gZahl) {
			gZahl = lZahl2;
		}
		if (lZahl3 > gZahl) {
			gZahl = lZahl3;
		}
		return gZahl;
	}

	public static char holeMittlereZahl(char lZahl1, char lZahl2, char lZahl3) {
		char mZahl = lZahl1;
		if (lZahl2 > mZahl && lZahl2 < lZahl3) {
			mZahl = lZahl2;
		}
		if (lZahl2 < mZahl && lZahl2 > lZahl3) {
			mZahl = lZahl2;
		}
		if (lZahl3 > mZahl && lZahl3 < lZahl2) {
			mZahl = lZahl3;
		}
		if (lZahl3 < mZahl && lZahl3 > lZahl2) {
			mZahl = lZahl3;
		}
		return mZahl;
	}
}
