package Traumschiff;

import java.util.ArrayList;

public class Raumschiff
{
	
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int anzahlDroiden;
	private String schiffname;
	private static ArrayList <String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList <Ladung> ladungsverzeichnis = new ArrayList<Ladung>();


	public Raumschiff()
	{
		
	}
	
	public Raumschiff(String schiffname, int photonentorpedoAnzahl,int energieversorgungInProzent, int zustandschildInProzent,int zustandHuelleInProzent,
					  int zustandLebenserhaltungssystemeInProzent, int anzahlDroiden)
	{
		this.schiffname = schiffname;
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = zustandschildInProzent;
		this.huelleInProzent = zustandHuelleInProzent;
		this.lebenserhaltungssystemeInProzent = zustandLebenserhaltungssystemeInProzent;
		this.anzahlDroiden = anzahlDroiden;
		
	}
	
	public void addLadung(Ladung neueLadung)
	{
		ladungsverzeichnis.add(neueLadung);
			  
	}
	
	public void photonentorpedoSchiessen(Raumschiff r)
	{
		if(photonentorpedoAnzahl==0)
		{
			System.out.println("-=*Click*=-!");
		}
		else
		{
			photonentorpedoAnzahl--;
			System.out.println("Photonentorpedo abgeschossen!");
			r.treffer(r);
		}
	}
	
	public void phaserkanoneSchiessen()
	{
		if(energieversorgungInProzent>=50)
		{
			System.out.println("-=*Click*=-!");
		}
		else
		{
			for(int i=0; i<50; i++)
			{
				energieversorgungInProzent--;
			}
			System.out.println("Phaserkanone abgeschossen");
			treffer(null);
		}
	}
	
	public void nachrichtAnAlle(String message)
	{
		broadcastKommunikator.add(message);
		System.out.println(message);
	}
	
	public static ArrayList<String> eintraegeLogbuchZurueckgeben()
	{
		return broadcastKommunikator;
	}
	
	public void photonentorpedosLaden(int anzahlTorpedos)
	{
		
	}
	
	public void reperaturDurchfuehren(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle, int anzahlDroiden)
	{
		
	}
	
	public void zustandRaumschiff()
	{		
		System.out.println("Zustandst Analyse des Schiffs: " + schiffname);
		System.out.println("Anzahl der Photoentorpedos: " + photonentorpedoAnzahl);
		System.out.println("Anzahl der Droiden: " + anzahlDroiden);
		System.out.println("Energieversorgung: " + energieversorgungInProzent + "%");
		System.out.println("Energieschilde: " + schildeInProzent + "%");
		System.out.println("Schiffh�lle: " + huelleInProzent + "%");
		System.out.println("Lebenserhaltungssysteme: " + lebenserhaltungssystemeInProzent + "%");
		System.out.println("Broadcast Kommunikator Channels: " + broadcastKommunikator);
		System.out.println("Ladungsverzeichnis: ");
		for (Ladung element : ladungsverzeichnis)
		{
			System.out.print(" " + element.getBezeichnung() + ",");
		}
		System.out.println();
		System.out.println();
	}
	
	public void ladungsverzeichnisAusgeben()
	{
		for (Ladung element : ladungsverzeichnis)
		{
			System.out.println(element.getBezeichnung()+ " " + element.getMenge());
		}
	}
	
	public void ladungsverzeichnisAufraeumen()
	{
		if(ladungsverzeichnis.isEmpty())
		{
			ladungsverzeichnis.clear();
		}
	}
	
	public void treffer(Raumschiff r)
	{		
		System.out.println(schiffname + "Wurde getroffen!");
		r.schildeInProzent = schildeInProzent-50;
		if(schildeInProzent >= 0)
		{
			huelleInProzent = huelleInProzent-50;
			energieversorgungInProzent = energieversorgungInProzent-50;
		}
		if(huelleInProzent >= 0)
		{
			lebenserhaltungssystemeInProzent = 0;
			System.out.println("Die Lebenserhaltungssystem sind vollst�ndig zerst�rt!");
		}
	}
	
	/**
	 ** Getters & Setters
	 ** @return
	 */
	
	public int	getPhotonentorpedoAnzahl()
	{
		return photonentorpedoAnzahl;
	}
	
	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahlNeu)
	{
		this.photonentorpedoAnzahl = photonentorpedoAnzahlNeu;
	}

	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	public void setEnergieversorgungInProzent(int energieversorgungInProzentNeu) {
		this.energieversorgungInProzent = energieversorgungInProzentNeu;
	}

	public int getSchildeInProzent() {
		return schildeInProzent;
	}

	public void setSchildeInProzent(int schildeInProzentNeu) {
		this.schildeInProzent = schildeInProzentNeu;
	}

	public int getHuelleInProzent() {
		return huelleInProzent;
	}

	public void setHuelleInProzent(int huelleInProzentNeu) {
		this.huelleInProzent = huelleInProzentNeu;
	}

	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}

	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzentNeu) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzentNeu;
	}

	public String getSchiffname() {
		return schiffname;
	}

	public void setSchiffname(String schiffname) {
		this.schiffname = schiffname;
	}

	public ArrayList<String> getBroadcastKommunikator() {
		return broadcastKommunikator;
	}

	public void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		this.broadcastKommunikator = broadcastKommunikator;
	}

	public ArrayList<Ladung> getLadungsverzeichnis() {
		return ladungsverzeichnis;
	}

	public void setLadungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
		this.ladungsverzeichnis = ladungsverzeichnis;
	}
	
	public int getAnzahlDroiden() {
		return anzahlDroiden;
	}

	public void setAnzahlDroiden(int anzahlDroiden) {
		this.anzahlDroiden = anzahlDroiden;
	}
	

}

