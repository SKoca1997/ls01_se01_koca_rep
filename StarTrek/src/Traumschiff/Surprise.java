package Traumschiff;

import java.util.ArrayList;

public class Surprise {

	public static void main(String[] args) {
		
		Raumschiff sternentrek = new Raumschiff("Enterprice", 100, 100, 100, 100, 100, 100);
		Ladung l1 = new Ladung("Schiffteile", 50);
		Ladung l2 = new Ladung("Nahrung", 150);
		
		
		sternentrek.addLadung(l1);
		sternentrek.addLadung(l2);
		sternentrek.zustandRaumschiff();
		sternentrek.ladungsverzeichnisAusgeben();
	}

}
