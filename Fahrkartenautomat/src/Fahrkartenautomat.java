﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
        Scanner tastatur = new Scanner(System.in);

        double zuZahlenderBetrag;
        double rueckgabebetrag;

          zuZahlenderBetrag = fahrkartenbestellungErfassen();
          rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
          fahrkartenAusgeben();
          rueckgeldAusgeben(rueckgabebetrag);
    }
    
    public static double fahrkartenbestellungErfassen()
    {
    	int anzahlTickets;
    	double ticketPreis;
    	char antwort = 'N';
    	Scanner tastatur = new Scanner(System.in);
    	
    	do
	    	{
	    	System.out.println("Ticketpreis (EURO-Cent): ");
	    	ticketPreis = tastatur.nextDouble();
	    	
	    	System.out.println("Wie viele Karten wollen Sie haben (Maximal 10 sind erlaubt)");
	    	anzahlTickets = tastatur.nextInt();
	
	        if (anzahlTickets <= 10 && anzahlTickets > 0)
	        {
	            System.out.println("Sie bestellen " + anzahlTickets + " Karten");
	        } else {
	            System.out.println("SYSTEM FEHLER-Falsche Eingabe");
	        }
        	System.out.println("\nMöchten sie die Eingabe bestätigen? Y/N");
        	antwort = tastatur.next().charAt(0);
        	
    	}while(antwort == 'N');
    	return ticketPreis * anzahlTickets;
    }
    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag)
    {
    	Scanner tastatur = new Scanner(System.in);
    	double eingezahlterGesamtbetrag = 0.0;
        while (eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
            System.out.format("Noch zu zahlen: %4.2f €%n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
            System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
            double eingeworfeneMuenze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMuenze;
        }
        return eingezahlterGesamtbetrag - zuZahlenderBetrag;
    }
    
    public static void fahrkartenAusgeben()
    {
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
            System.out.print("=");
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        System.out.println("\nFahrkarte ausgegeben!");
    }
    
    public static double rueckgeldAusgeben(double rueckgabebetrag)
    {
        System.out.println("\n\n");
        if (rueckgabebetrag > 0.0) {
            //***** Lösung der Ausgabenformatierungsaufgabe *****************/
            System.out.format("Der Rückgabebetrag in Höhe von %4.2f € %n", rueckgabebetrag);
            System.out.println("wird in folgenden Münzen ausgezahlt:");

            while (rueckgabebetrag >= 2.0) // 2 EURO-Münzen
            {
                System.out.println("2 EURO");
                rueckgabebetrag -= 2.0;
            }
            while (rueckgabebetrag >= 1.0) // 1 EURO-Münzen
            {
                System.out.println("1 EURO");
                rueckgabebetrag -= 1.0;
            }
            while (rueckgabebetrag >= 0.5) // 50 CENT-Münzen
            {
                System.out.println("50 CENT");
                rueckgabebetrag -= 0.5;
            }
            while (rueckgabebetrag >= 0.2) // 20 CENT-Münzen
            {
                System.out.println("20 CENT");
                rueckgabebetrag -= 0.2;
            }
            while (rueckgabebetrag >= 0.1) // 10 CENT-Münzen
            {
                System.out.println("10 CENT");
                rueckgabebetrag -= 0.1;
            }
            while (rueckgabebetrag >= 0.05)// 5 CENT-Münzen
            {
                System.out.println("5 CENT");
                rueckgabebetrag -= 0.05;
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
                + "Wir wünschen Ihnen eine gute Fahrt.");
        return rueckgabebetrag;
    }
    
}